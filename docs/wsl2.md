# WSL 2

O *Windows Subsystem for Linux* é uma forma de rodar o Linux nativamente no Windows, sem a necessidade de virtualização.

[Referencia](https://docs.microsoft.com/en-us/windows/wsl/)

## Networking

Para acessar os serviços do Linux pelo Windows, usar o IP do WLS. 

**Obter o IP**

```sh
ip addr | grep eth0
```

Para acessar os serviços do Windows no Linux, usar o IP do **nameserver**

No WSL:

```sh
cat /etc/resolv.conf
```

## Instalando o Docker

[Referência - Ubuntu](https://docs.docker.com/engine/install/ubuntu)

```sh
sudo apt-get update

sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin

sudo /etc/init.d/docker start

```

💡 Para subir o serviço docker, executar o comando `sudo service docker start`