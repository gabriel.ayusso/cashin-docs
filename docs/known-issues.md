# Erros Comuns

## no pg_hba.conf entry for host "xxx.xxx.xxx.xxx"

Esse erro acontece porque o Postgres bloqueia hosts desconhecidos por padrão.

Para corrigir, basta incluir o host no arquivo.

**Usuários WSL2**

O host do Linux (no windows) é diferente de local, por isso pode acontecer de o Postgres não reconhecer como um host confiável. 

Como esse IP pode mudar (até a classe C), o ideal seria incluir um range, que te permita conectar via Postgres por qualquer IP dentro desse range.

Por exemplo, se ele reclamar do IP `192.168.3.4`, isso significa que a cada reinicialização do sistema, o WSL pode assumir um IP entre 192.168.0.1 até 192.168.255.255. Para compreender todo esse range, podemos usar o IP com máscara: `192.168.0.1/24`.

Abrir o arquivo `<posgres instalation>/data/pg_hba.conf`, e incluir a seguinte linha (abaixo de todos)

```
# IPv4 WSL connections:
host    all             all             192.168.0.1/16           scram-sha-256
```

ℹ️  Após alterar o arquivo, não é preciso reiniciar o Postgres