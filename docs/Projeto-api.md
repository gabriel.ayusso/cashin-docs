# Configuração do Projeto API

- [Configuração do Projeto API](#configuração-do-projeto-api)
  - [Requisitos (Windows)](#requisitos-windows)
  - [Baixar o projeto. Link](#baixar-o-projeto-link)
  - [REDIS (Docker)](#redis-docker)
  - [Configurar o Env](#configurar-o-env)


## Requisitos (Windows)

* WSL2 (Estou usando Ubuntu 20)
* VS Code
* Postgres
* Node 12+ (uso yarn)
* 

## Baixar o projeto. [Link](https://gitlab.com/cashin1/cashin-api/api)

```sh
git clone git@gitlab.com:cashin1/cashin-api/api.git
```

## REDIS (Docker)

```sh
docker run -d --name my-redis -p 6379:6379 --restart always redis
```

## Configurar o Env

Criar o arquivo `.development.env`, com o seguinte conteúdo:

```conf
NODE_ENV=development
LOG_LEVEL=debug
CURR_ENV=local
PORT=3000
I18N_LOCALES=pt
I18N_DEFAULT_LOCALE=pt

REDIS_HOST=localhost

POSTGRES_PORT=5432
POSTGRES_HOST=********
POSTGRES_USERNAME=********
POSTGRES_PASSWORD=********
POSTGRES_DATABASE=********

JWT_SECRET_KEY=1234
JWT_EXPIRATION_TIME=86400

EMAIL_PAYBILL_TO=luciana@cashin.com.br
EMAIL_ADMIN=admin@cashin.com.br
EMAIL_FROM=suporte@cashin.com.br
EMAIL_FROM_NAME=Cash.In
EMAIL_DEBUG_TO=gabriel.ayusso@cashin.com.br ### USAR O SEU EMAIL ###
EMAIL_DEBUG_TO_NAME=Debug Cashin
EMAIL_STRATEGY=Sendgrid

SENDGRID_API_KEY=********

PAYBILL_FIXED_FEE=990
PAYBILL_VARIANT_FEE=1.1
PAYBILL_MIN_VALUE=20000

PLATFORM_COMPANY_URL=http://localhost:4020
PLATFORM_USER_URL=http://localhost:4030

INCENTIVALE_HOST=https://api-prd-01-hom.incentivale.com.br
INCENTIVALE_AUTH_USER=********
INCENTIVALE_AUTH_PASSWORD=********
INCENTIVALE_AUTH_GRANTS=********
INCENTIVALE_CAMPAIGN_TOKEN=********

CELCOIN_HOST=https://sandbox-apicorp.celcoin.com.br
CELCOIN_AUTH_USER=********
CELCOIN_AUTH_PASSWORD=********
CELCOIN_STATUS_QUEUE_LIMIT=50

PAYSMART_HOST=https://api-hml.paysmart.com.br/paySmart/ps-processadora/v1
PAYSMART_ACCOUNT_ID=********
PAYSMART_API_KEY=********
PAYSMART_ID_TRANSPORT_KEY=********
PAYSMART_OUTGOING_ZPK=********
PAYSMART_RSA_CERT=********
PAYSMART_RSA_KEY=********

QITECH_API_KEY=a480707b-b4ab-4b70-97b6-90852160a4b2
QITECH_HOST=https://api-auth.sandbox.qitech.app
QITECH_WALLET_ID=********
QITECH_CLIENT_PRIVATE_KEY=********
QITECH_PUBLIC_API_KEY=********

API_CALENDARIO_HOST=https://api.calendario.com.br/
API_CALENDARIO_TOKEN=********

STARK_BANK_PROJECT_ID=********
STARK_BANK_PROJECT_KEY=********

SMS_SERVICE=SNS

CELCOIN_CLIENT_ID=********
CELCOIN_CLIENT_SECRET=********
CRON_TOKEN=********
ID_BREAKAGE_WALLET=********
ID_GENERAL_ADMIN=********
MINU_SERVICE_HOST=********
REDIS_PORT=********
SCRIPT_MODE=********
```

