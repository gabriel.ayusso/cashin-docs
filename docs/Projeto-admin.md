# Projeto Admin

## Environment Variables

Ajustar o arquivo `.development.env` com as seguintes informações

⚠️ Caso esteja usando o WSL, a variável `REACT_APP_API_URL` deve corresponder ao IP do linux no WSL (IP4 da interface eth0).

```conf
PORT=4020
REACT_APP_VERSION=$npm_package_version
REACT_APP_API_URL=http://172.28.28.75:3000/graphql
REACT_APP_ANALYTICS_TRACKING_ID=UA-155149452-2
REACT_APP_CAPTCHA_KEY=
```