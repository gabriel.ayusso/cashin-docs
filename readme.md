# Cash In - Docs

![Logo](assets/logo-192x192.png)

Estou montando essa documentação para facilitar o onboarding e também servir como referência futura para todos. O objetivo desse projeto é centralizar as infos e detalhes sobre o desenvolvimento e compartilhar com todos.

# Sim, Cash In!

**Links úteis:**

* [WSL2](docs/wsl2.md)
* [Projeto API](docs/Projeto-api.md)
* [Erros Comuns](docs/known-issues.md)

**Conteúdo**

- [Cash In - Docs](#cash-in---docs)
- [Sim, Cash In!](#sim-cash-in)
  - [Windows](#windows)
  - [VS Code](#vs-code)
  - [GitLab](#gitlab)
    - [1. Se nào tiver uma chave ssh criada, criar uma (funciona no linux ou no windows/powershell 7+).](#1-se-nào-tiver-uma-chave-ssh-criada-criar-uma-funciona-no-linux-ou-no-windowspowershell-7)
    - [2. Obter a chave pública para adicionar no GitLab:](#2-obter-a-chave-pública-para-adicionar-no-gitlab)
    - [3. GitLab](#3-gitlab)


## Windows

Recomendo utilizar o WSL2 para trabalhar remotamente.

Eu estou usando o WSL 2 com **Ubuntu 20.04 LTS**. Pode utilizar sua distro preferida, desde que adapte os comandos para a distro escolhida.

## VS Code

**Remote WSL**

Para utilizar o VSCode remoto, no linux, é necessário instalar a extensão [Remote WSL](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl), da própria Microsoft.


**Markdown All in One**

Para atualizar esse document (e trabalhar com Markdown), recomendo a utilziação da extensão [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) do Yu Zhang, que oferece **muitos** recursos interessantes.

## GitLab

[Link](https://gitlab.com/cashin1)

Recomendo adicionar sua chave ssh no seu usuário. Isso vai tornar o acesso via git mais simples.

### 1. Se nào tiver uma chave ssh criada, criar uma (funciona no linux ou no windows/powershell 7+).

Para verifcar se há uma chave, execute o comando abaixo e verifique se existe os arquivos id_rsa e id_rsa.pub

```sh
ls -lha ~/.ssh/
```

Se não existir os arquivos (ou a pasta), o comando a seguir cria ambos para você.

```sh
ssh-keygen
```

### 2. Obter a chave pública para adicionar no GitLab:

Execute o comando e copie toda o conteúdo que ele ecoar.

```sh
cat ~/.ssh/id_rsa.pub
```

### 3. GitLab

Navegar pelas preferências de usuário, até a sessão [SSH Keys](https://gitlab.com/-/profile/keys), e colar o conteúdo que foi copiado no passo anterior em Key. Digitar um título (exemplo: `DELL Gabriel`), e salvar.