# Kubernetes and Docker

⚠️ Essa pasta foi criada inicialmente na raíz do projeto `api`.

## Comands and Tips

Criar um alias para o kubectl no minikube:

```sh
alias kubectl="minikube kubectl --"
```


**Build Docker**

Criar uma imagem Docker:

```sh
docker build -t cashin-api -f k8s/Dockerfile.api .
```

**Proxy Reverso**

```sh
docker run --name nginx -v /home/gabriel/src/api/k8s/nginx.conf:/etc/nginx/conf.d/nginx.conf -p 8080:8080 --network=host -d nginx
```